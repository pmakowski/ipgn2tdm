#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Corrige les altitudes d'un fichier gpx"""

__version__ = "0.0.1"

import argparse
import pathlib
import gpxpy
import srtm


def get_parser():
    ap = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
    )
    ap.add_argument("-v", "--version", action="version", version=__version__)
    ap.add_argument("-i", "--input", required=True, type=pathlib.Path, help="Fichier gpx à traiter")
    ap.add_argument(
        "-o", "--output", required=True, type=pathlib.Path, help="Nom de fichier de sortie"
    )
    ap.add_argument("-m", "--missing", default=False, action='store_true', help="points manquants seulement")
    return ap


def main():
    parser = get_parser()
    args = parser.parse_args()
    gpx = gpxpy.parse(open(args.input))
    elevation_data = srtm.get_data()
    elevation_data.add_elevations(gpx, only_missing=args.missing, smooth=True)
    for point in gpx.walk(only_points=True):
        point.elevation = round(point.elevation)
    with open(args.output, "w") as f:
        f.write(gpx.to_xml())


if __name__ == "__main__":
    main()
