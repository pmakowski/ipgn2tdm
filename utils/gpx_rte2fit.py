#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Transforme une route gpx en fichier fit
   Cela utilise gpxbabel.
"""

__version__ = "0.0.1"

import argparse
import pathlib
import subprocess
import tempfile
import gpxpy


def get_parser():
    ap = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    ap.add_argument("-v", "--version", action="version", version=__version__)
    ap.add_argument("-i", "--input", required=True, type=pathlib.Path, help="Fichier gpx à traiter")
    return ap


def main():
    parser = get_parser()
    args = parser.parse_args()
    gpx_in = gpxpy.parse(open(args.input))
    gpx_out = gpxpy.gpx.GPX()
    gpx_track = gpxpy.gpx.GPXTrack()
    gpx_track.name = gpx_in.routes[0].name
    gpx_out.tracks.append(gpx_track)
    gpx_segment = gpxpy.gpx.GPXTrackSegment()
    gpx_track.segments.append(gpx_segment)
    for waypoint in gpx_in.waypoints:
        waypoint.elevation = round(waypoint.elevation)
        gpx_out.waypoints.append(waypoint)
    for point in gpx_in.routes[0].walk(only_points=True):
        point.elevation = round(point.elevation)
        gpx_segment.points.append(point)
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as f:
        f.write(gpx_out.to_xml())
        f.close()
        subprocess.run(
            [
                "/usr/bin/gpsbabel",
                "-i",
                "gpx",
                "-o",
                "garmin_fit",
                f.name,
                pathlib.Path(args.input).with_suffix(".fit"),
            ]
        )
        pathlib.Path(f.name).unlink(missing_ok=True)


if __name__ == "__main__":
    main()
