#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Transforme une trace en route fichier gpx"""

__version__ = "0.0.1"

import argparse
import pathlib
import gpxpy


def get_parser():
    ap = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    ap.add_argument("-v", "--version", action="version", version=__version__)
    ap.add_argument("-i", "--input", required=True, type=pathlib.Path, help="Fichier gpx à traiter")
    ap.add_argument(
        "-o", "--output", required=True, type=pathlib.Path, help="Nom de fichier de sortie"
    )
    ap.add_argument("-n", "--name", required=True, type=str, help="Nom de la route")
    return ap


def main():
    parser = get_parser()
    args = parser.parse_args()
    gpx_in = gpxpy.parse(open(args.input))
    gpx_out = gpxpy.gpx.GPX()
    gpx_rte = gpxpy.gpx.GPXRoute(name=args.name)
    gpx_out.routes.append(gpx_rte)
    for waypoint in gpx_in.waypoints:
        waypoint.elevation = round(waypoint.elevation)
        gpx_out.waypoints.append(waypoint)
    for point in gpx_in.walk(only_points=True):
        point.elevation = round(point.elevation)
        gpx_rte.points.append(point)
    with open(args.output, "w") as f:
        f.write(gpx_out.to_xml())


if __name__ == "__main__":
    main()
