#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Convert FIT files (from Garmin) to GPX files"""

__version__ = "0.0.1"

import argparse
import os
import pathlib
from datetime import datetime, timedelta
from typing import Dict, Union, Optional, Tuple
import pandas as pd
import gpxpy.gpx
import fitdecode
import xml.etree.ElementTree as mod_etree


class Converter:
    """Main converter that holds the FIT > pd.DataFrame and pd.DataFrame > GPX methods"""

    def __init__(self, status_msg: bool = False):
        """Main constructor for Converter
        Parameters:
            status_msg (bool): Option to have the Converter print to console with status messages,
            such as number of files converted.
        """
        self.status_msg = status_msg
        # The names of the columns we will use in our points and laps DataFrame
        # (use the same name as the field names in FIT file to facilate parsing)
        self._colnames_points = [
            "latitude",
            "longitude",
            "lap",
            "altitude",
            "timestamp",
            "heart_rate",
            "cadence",
            "speed",
            "temperature",
        ]

        self._colnames_laps = [
            "number",
            "start_time",
            "total_distance",
            "total_elapsed_time",
            "max_speed",
            "max_heart_rate",
            "avg_heart_rate",
        ]

        # Note: get_fit_laps(), get_fit_points(), get_dataframes() are shamelessly copied (and adapted) from:
        # https://github.com/bunburya/fitness_tracker_data_parsing/blob/main/parse_fit.py

    def _get_fit_laps(
        self, frame: fitdecode.records.FitDataMessage
    ) -> Dict[str, Union[float, datetime, timedelta, int]]:
        """Extract some data from a FIT frame representing a lap and return it as a dict."""
        # Step 0: Initialise data output
        data: Dict[str, Union[float, datetime, timedelta, int]] = {}

        # Step 1: Extract all other fields
        #  (excluding 'number' (lap number) because we don't get that from the data but rather count it ourselves)
        for field in self._colnames_laps[1:]:
            if frame.has_field(field):
                data[field] = frame.get_value(field)

        return data

    def _get_fit_points(
        self, frame: fitdecode.records.FitDataMessage
    ) -> Optional[Dict[str, Union[float, int, str, datetime]]]:
        """Extract some data from an FIT frame representing a track point and return it as a dict."""
        # Step 0: Initialise data output
        data: Dict[str, Union[float, int, str, datetime]] = {}

        # Step 1: Obtain frame lat and long and convert it from integer to degree (if frame has lat and long data)
        if not (frame.has_field("position_lat") and frame.has_field("position_long")):
            # Frame does not have any latitude or longitude data. Ignore these frames in order to keep things simple
            return None
        elif frame.get_value("position_lat") is None and frame.get_value("position_long") is None:
            # Frame lat or long is None. Ignore frame
            return None
        else:
            data["latitude"] = frame.get_value("position_lat") / ((2 ** 32) / 360)
            data["longitude"] = frame.get_value("position_long") / ((2 ** 32) / 360)

        # Step 2: Extract all other fields
        for field in self._colnames_points[3:]:
            if frame.has_field(field):
                data[field] = frame.get_value(field)
        if frame.has_field("enhanced_altitude"):
            data["altitude"] = frame.get_value("enhanced_altitude")
        if frame.has_field("enhanced_speed"):
            data["speed"] = frame.get_value("enhanced_speed")

        return data

    def fit_to_dataframes(self, fname: str) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Takes the path to a FIT file and returns two Pandas DataFrames for lap data and point data
        Parameters:
            fname (str): string representing file path of the FIT file
        Returns:
            dfs (tuple): df containing data about the laps, df containing data about the individual points.
        """
        # Check that this is a .FIT file
        input_extension = os.path.splitext(fname)[1]
        if input_extension.lower() != ".fit":
            raise fitdecode.exceptions.FitHeaderError("Input file must be a .FIT file.")

        data_points = []
        data_laps = []
        lap_no = 1
        with fitdecode.FitReader(fname) as fit_file:
            for frame in fit_file:
                if isinstance(frame, fitdecode.records.FitDataMessage):
                    # Determine if frame is a data point or a lap:
                    if frame.name == "record":
                        single_point_data = self._get_fit_points(frame)
                        if single_point_data is not None:
                            single_point_data["lap"] = lap_no  # record lap number
                            data_points.append(single_point_data)

                    elif frame.name == "lap":
                        single_lap_data = self._get_fit_laps(frame)
                        single_lap_data["number"] = lap_no
                        data_laps.append(single_lap_data)
                        lap_no += 1  # increase lap counter

        # Create DataFrames from the data we have collected.
        # (If any information is missing from a lap or track point, it will show up as a "NaN" in the DataFrame.)

        df_laps = pd.DataFrame(data_laps, columns=self._colnames_laps)
        df_laps.set_index("number", inplace=True)
        df_points = pd.DataFrame(data_points, columns=self._colnames_points)

        return df_laps, df_points

    # Method adapted from: https://github.com/nidhaloff/gpx-converter/blob/master/gpx_converter/base.py
    def dataframe_to_gpx(
        self,
        df_points,
        col_lat="latitude",
        col_long="longitude",
        col_time=None,
        col_alt=None,
        gpx_name=None,
        gpx_desc=None,
        gpx_link=None,
        gpx_type=None,
    ):
        """
        Convert a pandas dataframe to GPX
        Parameters:
            df_points (pd.DataFrame): pandas dataframe containing at minimum lat and long info of points
            col_alt (str): name of the altitudes column
            col_time (str): name of the time column
            col_long (str): name of the longitudes column
            col_lat (str): name of the latitudes column
            gpx_name (str): name for the gpx track (note is not the same as the file name)
            gpx_desc (str): description for the gpx track
            gpx_type : activity type for the gpx track (can be str, or int)
            gpx_link (str): link to the gpx activity
        """
        # Step 0: Check that the input dataframe has all required columns
        cols_to_check = [col_lat, col_long]
        if col_alt:
            cols_to_check.append(col_alt)
        if col_time:
            cols_to_check.append(col_time)

        if any(elem not in df_points.columns for elem in cols_to_check):
            raise KeyError(
                "The input dataframe must consist of point coordinates in longitude and latitude. "
                "Ideally, it should be the df_points output from the fit_to_dataframes() method."
            )

        # Step 1: Initiate GPX object
        gpx = gpxpy.gpx.GPX()
        # -- create first track in our GPX:
        gpx_track = gpxpy.gpx.GPXTrack()
        gpx.tracks.append(gpx_track)
        # -- create first segment in our GPX track:
        gpx_segment = gpxpy.gpx.GPXTrackSegment()
        gpx_track.segments.append(gpx_segment)

        # Step 2: Assign GPX track metadata
        gpx.tracks[0].name = gpx_name
        gpx.tracks[0].type = gpx_type
        gpx.tracks[0].description = gpx_desc if not pd.isna(gpx_desc) else None
        gpx.tracks[0].link = gpx_link

        # Step 3: Add points from dataframe to GPX track:
        # definition of extension
        namespace = "{gpxtpx}"
        nsmap = {namespace[1:-1]: "http://www.garmin.com/xmlschemas/TrackPointExtension/v1"}
        gpx.nsmap = nsmap

        for idx in df_points.index:
            # Create trackpoint:
            track_point = gpxpy.gpx.GPXTrackPoint(
                latitude=df_points.loc[idx, col_lat],
                longitude=df_points.loc[idx, col_long],
                time=pd.Timestamp(df_points.loc[idx, col_time]) if col_time else None,
                elevation=df_points.loc[idx, col_alt] if col_alt else None,
            )
            # create extension element
            root = mod_etree.Element(namespace + "TrackPointExtension")
            hr = mod_etree.SubElement(root, namespace + "hr")
            hr.text = str(df_points.loc[idx, "heart_rate"])
            temperature = mod_etree.SubElement(root, namespace + "atemp")
            temperature.text = str(df_points.loc[idx, "temperature"])
            track_point.extensions.append(root)
            # Append GPX_TrackPoint to segment:
            gpx_segment.points.append(track_point)

        return gpx

    def fit_to_gpx(self, f_in, f_out):
        """Method to convert a FIT file into a GPX file
        Parameters:
            f_in (str): file path to FIT activity
            f_out (str): file path to save the converted FIT file
        """
        # Step 0: Validate inputs
        input_extension = os.path.splitext(f_in)[1]
        if input_extension != ".fit":
            raise TypeError("Input file must be a .FIT file.")

        output_extension = os.path.splitext(f_out)[1]
        if output_extension != ".gpx":
            raise TypeError("Output file must be a .gpx file.")

        # Step 1: Convert FIT to pd.DataFrame
        df_laps, df_points = self.fit_to_dataframes(f_in)

        # Step 2: Convert pd.DataFrame to GPX
        gpx = self.dataframe_to_gpx(
            df_points=df_points,
            col_lat="latitude",
            col_long="longitude",
            col_time="timestamp",
            col_alt="altitude",
        )

        # Step 3: Save file
        with open(f_out, "w") as f:
            f.write(gpx.to_xml())

        return gpx

    def fit_to_gpx_bulk(self, dir_in, dir_out):
        """Method to convert all FIT files in a directory to GPX files
        Parameters:
            dir_in (str): path to directory with all FIT activities
            dir_out (str): path to directory to save the converted FIT files to
        """

        # Validate inputs (check that the input and output directories end with '/')
        if dir_in[-1] != "/":
            dir_in += "/"

        if dir_out and dir_out[-1] != "/":
            dir_out += "/"

        # -- check output directory exists, otherwise make dir
        if not os.path.isdir(dir_out):
            os.mkdir(dir_out)

        # Iterate through all files in indicated directory:
        # -- identify fit files
        fit_files = [f for f in os.listdir(dir_in) if ".fit" in f.lower()]

        for f_activity in fit_files:
            gpx_file = dir_out + os.path.splitext(f_activity)[0] + ".gpx"
            if not os.path.exists(gpx_file):
                # Convert file and save to file
                self.fit_to_gpx(f_in=dir_in + f_activity, f_out=gpx_file)


def get_parser():
    ap = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    ap.add_argument("--version", action="version", version=__version__)
    group1 = ap.add_mutually_exclusive_group(required=True)
    group1.add_argument("--fit-file", type=pathlib.Path, help="Fichier FIT à traiter")
    group1.add_argument("--fit-dir", type=pathlib.Path, help="Répertoire des fichiers FIT")
    group2 = ap.add_mutually_exclusive_group(required=True)
    group2.add_argument("--gpx-file", type=pathlib.Path, help="Nom de fichier de sortie")
    group2.add_argument("--gpx-dir", type=pathlib.Path, help="Répertoire des fichiers gpx")
    return ap


def main():
    parser = get_parser()
    args = parser.parse_args()
    if args.fit_file and args.gpx_file:
        try:
            conv = Converter()
            conv.fit_to_gpx(f_in=args.fit_file, f_out=args.gpx_file)
        except TypeError as e:
            print(e)
    elif args.fit_dir and args.gpx_dir:
        try:
            conv = Converter()
            conv.fit_to_gpx_bulk(dir_in=str(args.fit_dir), dir_out=str(args.gpx_dir))
        except TypeError as e:
            print(e)
    else:
        print("Indiquer soit fit-file et gpx-file, soit fit-dir et gpx-dir")


if __name__ == "__main__":
    main()
